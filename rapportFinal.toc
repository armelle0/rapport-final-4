\select@language {french}
\contentsline {section}{Introduction}{3}
\contentsline {section}{\numberline {I}Description du projet et objectif}{3}
\contentsline {section}{\numberline {II}Avancement au cours de l'ann\IeC {\'e}e}{4}
\contentsline {section}{\numberline {III}Projet d'utilisation des logiciels IBM : SPSS et ODM}{5}
\contentsline {subsection}{\numberline {III.1}Pr\IeC {\'e}sentation succincte de SPSS}{5}
\contentsline {subsection}{\numberline {III.2}Pourquoi nous avons abandonn\IeC {\'e} SPSS}{5}
\contentsline {subsection}{\numberline {III.3}Pr\IeC {\'e}sentation et utilisation d'ODM}{6}
\contentsline {subsection}{\numberline {III.4}Difficult\IeC {\'e}s avec ODM}{6}
\contentsline {section}{\numberline {IV}Analyse th\IeC {\'e}orique du probl\IeC {\`e}me et mod\IeC {\'e}lisation}{7}
\contentsline {subsection}{\numberline {IV.1}D\IeC {\'e}termination statistique du th\IeC {\`e}me des messages}{7}
\contentsline {subsubsection}{\numberline {IV.1.1}Normalisation du texte}{7}
\contentsline {subsubsection}{\numberline {IV.1.2}Description de l'algorithme TfidfVectorizer}{7}
\contentsline {subsubsection}{\numberline {IV.1.3}Classification des tweets par th\IeC {\`e}me - Entropie maximum}{8}
\contentsline {paragraph}{Choix de la m\IeC {\'e}thode et de la biblioth\IeC {\`e}que}{8}
\contentsline {paragraph}{Notations}{9}
\contentsline {paragraph}{Mod\IeC {\`e}le logarithme-lin\IeC {\'e}aire}{9}
\contentsline {paragraph}{Estimation des coefficients}{10}
\contentsline {paragraph}{Quel rapport avec l'entropie?}{11}
\contentsline {subsection}{\numberline {IV.2}D\IeC {\'e}terminer le profil de l'utilisateur}{12}
\contentsline {subsubsection}{\numberline {IV.2.1}Principe d'\IeC {\'e}laboration du profil en 5 \IeC {\'e}tapes}{13}
\contentsline {subsubsection}{\numberline {IV.2.2}D\IeC {\'e}terminer le lien entre deux utlisateurs u et v}{14}
\contentsline {subsubsection}{\numberline {IV.2.3}Apprentissage statistique}{15}
\contentsline {paragraph}{Base de donn\IeC {\'e}es}{16}
\contentsline {paragraph}{Quelques m\IeC {\'e}thodes d'apprentissage statistique autre que la r\IeC {\'e}gression lin\IeC {\'e}aire}{16}
\contentsline {paragraph}{La r\IeC {\'e}gression lin\IeC {\'e}aire au service de la d\IeC {\'e}termination du profil d'un utilisateur}{16}
\contentsline {subsection}{\numberline {IV.3}Traitement des messages (Tweets)}{17}
\contentsline {subsubsection}{\numberline {IV.3.1}Pertinence d'un tweet}{17}
\contentsline {subsubsection}{\numberline {IV.3.2}Classification}{19}
\contentsline {section}{\numberline {V}Construction d'une base de donn\IeC {\'e}es}{20}
\contentsline {section}{\numberline {VI} Impl\IeC {\'e}mentation de l'application}{21}
\contentsline {subsection}{\numberline {VI.1}Java}{21}
\contentsline {subsection}{\numberline {VI.2}Utilisation des biblioth\IeC {\`e}ques Python}{21}
\contentsline {subsubsection}{\numberline {VI.2.1}Motivations}{21}
\contentsline {subsubsection}{\numberline {VI.2.2}Probl\IeC {\`e}mes d'int\IeC {\'e}grations Jython}{21}
\contentsline {subsection}{\numberline {VI.3}Impl\IeC {\'e}mentation en Java du traitement des messages}{22}
\contentsline {subsubsection}{\numberline {VI.3.1}Biblioth\IeC {\`e}ques Java : Stanford Classifier et Snowball}{22}
\contentsline {subsubsection}{\numberline {VI.3.2}Structure du projet Java}{22}
\contentsline {paragraph}{TwMParser}{22}
\contentsline {paragraph}{AbstractTweetParser}{22}
\contentsline {paragraph}{classifier}{23}
\contentsline {paragraph}{Tweet}{23}
\contentsline {paragraph}{themes}{23}
\contentsline {paragraph}{LinearRegression}{23}
\contentsline {paragraph}{profil}{23}
\contentsline {paragraph}{LastTweets}{23}
\contentsline {subsection}{\numberline {VI.4}Une application sous android}{23}
\contentsline {subsubsection}{\numberline {VI.4.1}Fonctionnalit\IeC {\'e}s principales de l'application}{24}
\contentsline {subsubsection}{\numberline {VI.4.2}Fonctionnement g\IeC {\'e}n\IeC {\'e}ral de l'application}{24}
\contentsline {subsubsection}{\numberline {VI.4.3}Int\IeC {\'e}gration des algorithmes de classement des Tweets et d'\IeC {\'e}laboration du profil}{25}
\contentsline {paragraph}{Contraintes mat\IeC {\'e}rielles, vitesse d'\IeC {\'e}x\IeC {\'e}cution}{25}
\contentsline {paragraph}{Contraintes li\IeC {\'e}es \IeC {\`a} Twitter}{26}
\contentsline {subsection}{\numberline {VI.5}Extraction des donn\IeC {\'e}es}{27}
\contentsline {subsubsection}{\numberline {VI.5.1}Connexion \IeC {\`a} Twitter}{27}
\contentsline {subsubsection}{\numberline {VI.5.2}Quelles donn\IeC {\'e}es extrait-on ?}{27}
\contentsline {subsubsection}{\numberline {VI.5.3}Stockage des donn\IeC {\'e}es}{27}
\contentsline {section}{\numberline {VII} R\IeC {\'e}sultats}{28}
\contentsline {subsection}{\numberline {VII.1}Efficacit\IeC {\'e} du classifieur}{28}
\contentsline {subsection}{\numberline {VII.2}Construction du profil}{28}
\contentsline {paragraph}{Variation de $\alpha $}{29}
\contentsline {paragraph}{Variation de $\beta $}{29}
\contentsline {paragraph}{Variation de $R_w$}{29}
\contentsline {paragraph}{Variation de $W_{ff}$}{29}
\contentsline {subsection}{\numberline {VII.3}Resultats finaux visibles par l'utilisateur}{30}
\contentsline {section}{Conclusion}{32}
\contentsline {section}{Bibliographie}{33}
